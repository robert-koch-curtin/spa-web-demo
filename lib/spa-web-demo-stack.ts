import { Stack, StackProps } from "aws-cdk-lib";
import { Construct } from "constructs";
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import { FrontEnd } from "@assembly-line/cdk-base-lib/lib/patterns/front-end";
import { Source } from "aws-cdk-lib/aws-s3-deployment";

export class SpaWebDemoStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'SpaWebDemoQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });

    new FrontEnd(this, "SpaWebDemoFrontEnd", {
      appConfig: {
        appName: "wpa-demo",
        envName: "test",
        name: "wpa-demo",
      },
      frontEndAppSource: Source.asset("./website-dist"),
    });
  }
}

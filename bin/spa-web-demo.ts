#!/usr/bin/env node
import "source-map-support/register";
import { PipelineBuilder } from "@assembly-line/test/lib/NodePipeline";
import * as cdk from "aws-cdk-lib";
import { SpaWebDemoStack } from "../lib/spa-web-demo-stack";
import { Stack } from "aws-cdk-lib";
import { Construct } from "constructs";

const app = new cdk.App();

PipelineBuilder.createStacks(
  app,
  [
    {
      stackId: "FrontendDeploy",
      preApproval: false,
      stageName: "frontend-stage",
      createStack: (scope: Construct, id: string): Stack => {
        return new SpaWebDemoStack(scope, `${id}-UI`);
      },
    },
  ],
  {
    fileName: "./config/pipelineConfig.yml",
    pipelineIdPrefix: "SPA_DEMO_App-",
  }
);
